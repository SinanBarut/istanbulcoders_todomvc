FROM node:8

RUN npm install serve -g

COPY build/ build/

EXPOSE 5000

CMD ["serve","build"]
